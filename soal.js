// countUniqueValues([1, 2, 3, 4, 4, 4, 7, 7, 12, 12, 13])) => 7
// countUniqueValues([1, 2, 2, 2, 2, 2, 2, 2, 4, 6])) => 4
// countUniqueValues([])) =>  0

/* cara 1 */
function countUniqueValues(input){
    let uniq = [...new Set(input)];
    return uniq.length
}

let input = [1, 2, 3, 4, 4, 4, 7, 7, 12, 12, 13];
let input2 = [1, 2, 2, 2, 2, 2, 2, 2, 4, 6];

console.log(countUniqueValues(input));
console.log(countUniqueValues(input2));
console.log(countUniqueValues([]));

/* cara 2 */
function getUnique(input){
    let uniqueNumber = [];
    input.forEach((item) => {
    if (!uniqueNumber.includes(item)) {
        uniqueNumber.push(item);
    }
    });
    return uniqueNumber.length
}

let number = [1, 2, 3, 4, 4, 4, 7, 7, 12, 12, 13];
let number2 = [1, 2, 2, 2, 2, 2, 2, 2, 4, 6];

console.log(getUnique(number));
console.log(getUnique(number2));
console.log(getUnique([]));

/* cara 3 */
function cariUnique(input){
    input.sort();
    let uniq = []
    for (let i = 0; i < input.length; i++) {
        if(input[i] != input[i + 1]){
            uniq.push(i)
        } 
    }
    return uniq.length
}

console.log(cariUnique([1, 2, 3, 4, 4, 4, 7, 7, 12, 12, 13]));
console.log(cariUnique([1, 2, 2, 2, 2, 2, 2, 2, 4, 6]));
console.log(cariUnique([]));